# TP5 - Serveur HTTP et certificats TLS-SSL

## Question 1 : Grâce à Apache2 (et à la commande "install apache2") Apache 2 créé deux sites préconfigurés : « default » et « default-ssl » qui pointent tous les deux vers le répertoire « /var/www » mais le premier écoute sur le port 80 (HTTP) et le second sur le port 443 (HTTPS). Vous pouvez vérifier la statut de votre serveur grâce à la commande "systemctl status apache2" où en allant directement voir l’adresse du serveur.

```bash
sudo apt install apache2
sudo nano /etc/apache2/sites-available/default.conf
sudo nano /etc/apache2/sites-available/default-ssl.conf
sudo a2ensite default
sudo a2ensite default-ssl
sudo systemctl reload apache2
sudo systemctl status apache2
```

```
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

```
<VirtualHost *:443>
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    SSLEngine on
    SSLCertificateFile /etc/ssl/certs/ssl-cert-snakeoil.pem
    SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key
</VirtualHost>
```

## Question 2 : Vu que le site par défaut SSL, il est pré-configuré pour fonctionner. De ce fait, il suffit d’effectuer deux choses pour le rendre actif et opérationnel :

1. Activer le module SSL d’Apache (à l’aide de a2enmod)

```bash
sudo a2enmod ssl
```

2. Activer le site « default-ssl » d’Apache (à l’aide de a2ensite)

```bash
sudo a2ensite default-ssl
```

3. Recharger Apache

```bash
sudo systemctl reload apache2
```

## Question 3 : Grâce à OpenSSL, générer un certificat X.509 auto-signé. Attention à la gestion de vos clés privé et public.

```bash
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt
```

## Question 4 : Modifier le fichier de configuration du site « default-ssl » pour qu’il utilise le certificat auto-signé que vous venez de générer.

```bash
sudo nano /etc/apache2/sites-available/default-ssl.conf
```

```
<VirtualHost *:443>
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    SSLEngine on
    SSLCertificateFile /etc/ssl/certs/apache-selfsigned.crt
    SSLCertificateKeyFile /etc/ssl/private/apache-selfsigned.key
```

```bash
sudo systemctl reload apache2
```

## Question 5 : En utilisant la commande "htpasswd", créer un système d’authentification pour votre serveur (La confi-guration du serveur sera également à modifier). Créer plusieurs exemples d’utilisateurs et de mots de passes.

```bash
sudo apt install apache2-utils
sudo htpasswd -c /etc/apache2/.htpasswd guyaume
```

```bash
sudo nano /etc/apache2/sites-available/default-ssl.conf
```

```
<VirtualHost *:443>
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    SSLEngine on
    SSLCertificateFile /etc/ssl/certs/apache-selfsigned.crt
    SSLCertificateKeyFile /etc/ssl/private/apache-selfsigned.key
    <Directory "/var/www/html">
        AuthType Basic
        AuthName "Restricted Content"
        AuthUserFile /etc/apache2/.htpasswd
        Require valid-user
    </Directory>
</VirtualHost>
```

## Question 6 : Proposer des mesures/protocoles à mettre en place sur votre serveur pour en augmenter sa sécurité, puis, implémenter vos propositions.

Il serait envisageable de rediriger les requêtes HTTP vers HTTPS.