# Code Hamming

def index_to_bits(index, size):
    bits = []
    for _ in range(size):
        bits.append(0)
    for i in reversed(range(size)):
        power = 2 ** i
        if index >= power:
            index = index - power
            bits[i] = 1
    return bits



def bits_to_index(bits, size):
    index = 0
    for i in range(size):
        index = index + bits[i] * (2 ** i)
    return index 



def encode_hamming(a):
    u = []
    for _ in range(7):
        u.append(0)
    
    u[2] = a[0]
    u[4] = a[1]
    u[5] = a[2]
    u[6] = a[3]

    u[0] = a[0] ^ a[1] ^ a[3]
    u[1] = a[0] ^ a[2] ^ a[3]
    u[3] = a[1] ^ a[2] ^ a[3]

    return u

def decode_hamming(v):
    s = []
    for _ in range(3):
        s.append(0)
    
    s[0] = v[0] ^ v[2] ^ v[4] ^ v[6]
    s[1] = v[1] ^ v[2] ^ v[5] ^ v[6]
    s[2] = v[3] ^ v[4] ^ v[5] ^ v[6]

    s = bits_to_index(s,3)

    if s > 0 :
        print("Détection d'une erreur sur " + str(v) + " à la position " + str(s) + " !")
        v[s - 1] = 1 ^ v[s - 1]
    
    a = []
    for _ in range(4):
        a.append(0)
    
    a[0] = v[2]
    a[1] = v[4]
    a[2] = v[5]
    a[3] = v[6]

    return a

print(decode_hamming([1, 1, 0, 0, 1, 1, 0]))
print(decode_hamming([1, 1, 0, 0, 1, 0, 0]))
print(decode_hamming([0, 1, 0, 0, 1, 1, 0]))
print(decode_hamming([1, 1, 0, 0, 1, 1, 1]))