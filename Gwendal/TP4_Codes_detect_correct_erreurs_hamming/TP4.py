# 1 - Code détecteur d'erreur

# Question 1. 
# Implémenter une fonction prenant en entrée un message a et renvoyant le résultat 
# après application du code de parité.

def parity_code(a): # amélioration en faisant modulo 4, 8 ...
    nb1 = 0
    for i in range(len(a)):
        if a[i] == '1':
            nb1 += 1
    if nb1 % 2 == 1:
        return a + '1'
    else:
        return a + '0'

# messages de 7 bits
print(parity_code('1010101')) # 10101010
print(parity_code('1100111')) # 11001111

# Question 2. 
# Proposer et implémenter une fonction se basant sur le code de parité et 
# étant plus performant pour la détection d’une multitude d’erreurs.

def detect_many_errors(message):
    list_blocs_7 = []
    for i in range(0, len(message), 7):
        list_blocs_7.append(message[i:i+7])
    
    list_parities = []
    for bloc in list_blocs_7:
        list_parities.append(parity_code(bloc))
    
    return ''.join(list_parities)

print(detect_many_errors('10101011100111'))


# Question 3. Implémenter un fonction prenant en entrée un message a et 
# un paramètre k et renvoyant le résultat après application du code de k répétitions.
def repetition_code(a, k):
    return a * k

print(repetition_code('101', 3)) # 101101101

# Question 4. Implémenter un fonction prenant en entrée un message a et renvoyant 
# le résultat après application du code de Hamming.
def hamming_code(a):
    u3 = a[3]
    u5 = a[5]
    u6 = a[6]  
    u7 = a[7]

    u1 = str(int(u3) ^ int(u5) ^ int(u7))
    u2 = str(int(u3) ^ int(u6) ^ int(u7))
    u4 = str(int(u5) ^ int(u6) ^ int(u7))

    return u1 + u2 + u3 + u4 + u5 + u6 + u7
    
print(hamming_code('11101100'))

# 2 - Code correcteur d’erreur

# Question 5. 
# Supposons que l’on reçoit un message v condition par un code à k répétition 
# et qui a pu être altéré lors de l’envoie. 
# Implémenter une fonction permettant de retrouver le message originel u.

def repetition_decode(v, k):
    u = ""
    for i in range(k):
        u += v[i]
    return u

print(repetition_decode('101101101', 3)) # 101

# Question 6. 
# Supposons que l’on reçoit un message v de 7 bits conditionné par un code de Hamming
# et qui a pu être altéré de 1 bit lors de l’envoie. 
# Implémenter une fonction permettant de retrouver le message originel u

def hamming_decode(v):
    u3 = v[2]
    u5 = v[4]
    u6 = v[5]  
    u7 = v[6]

    u1 = str(int(u3) ^ int(u5) ^ int(u7))
    u2 = str(int(u3) ^ int(u6) ^ int(u7))
    u4 = str(int(u5) ^ int(u6) ^ int(u7))

    error = int(u4 + u2 + u1, 2)
    if error == 0:
        return v[2] + v[4] + v[5] + v[6]
    else:
        return v[:error-1] + str(int(v[error-1]) ^ 1) + v[error:]
    
print(hamming_decode('11101100')) # 1110

# Question 7. 
# Même question pour un message v de taille quelconque dans lequel au plus 1 bit est
# altéré tous les 7 bits.

def hamming_decode_v2(v):
    return None