# Protocole pour assurer l’authenticité d’un fichier envoyé sur un réseau
# A envoie img + hash(img) à B
# B hash(img) et compare avec hash(img) reçu de A

import hashlib
import secrets

# Fonction pour hacher un mot de passe avec du sel
def hash_password(password, salt):
    salted_password = password.encode() + salt.encode()
    hashed_password = hashlib.sha256(salted_password).hexdigest()
    return hashed_password

# Pour chaque mot de passe de password.txt
with open('/home/gleguellec/Desktop/password.txt', 'r') as file:
    for line in file:
        password = line.strip()
        # Hacher le mot de passe
        hashed_password = hashlib.sha256(password.encode()).hexdigest()
        
        # Ecrire le mot de passe haché dans un fichier
        with open('/home/gleguellec/Desktop/hashed_password.txt', 'a') as file:
            file.write(hashed_password + '\n')

        with open('/home/gleguellec/Desktop/salted_password.txt', 'a') as file:
            # Générer un sel aléatoire
            salt = secrets.token_hex(16)
            salted_hashed_password = hash_password(password, salt)
            # Ecrire le mot de passe haché et salé dans un fichier
            file.write(salted_hashed_password + '\n')