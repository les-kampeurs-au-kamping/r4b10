import hashlib

very_useful_passwords = ['123456', 'admin' , '1234', 'password', '123', 'Aa123456', '111111', '000000', 'admin123', 'root']

# hash each password in the list
for password in very_useful_passwords:
    hashed_password = hashlib.sha256(password.encode()).hexdigest()

    # write the hashed password in a file
    with open('/home/gleguellec/Desktop/rainbow_table.txt', 'a') as file:
        file.write(hashed_password + '\t' + password + '\n')