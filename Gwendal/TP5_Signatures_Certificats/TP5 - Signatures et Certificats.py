# Question 1

############################### Message et signature ###############################
# protocole simplifié sans hachage m est équivalent à h(m)
m=2 # message

d=23 # clé privée

# clé publique
e, n = (7, 55)

def rsa_sign(m, d, n):
    return pow(m, d, n) #h(m) -> h(m)^d mod n

signature = rsa_sign(m, d, n)
print("Message:", m, " Signature:", signature)

############################### Vérification de la signature ###############################

m=8 # signature

# clé publique
e, n = (7, 55)

def rsa_verify(m, e, n):
    return pow(m, e, n) # h(m)^e mod n -> h(m)

print("message initial", m, "verif", rsa_verify(m, e, n)) # 8^7 mod 55 = 2
