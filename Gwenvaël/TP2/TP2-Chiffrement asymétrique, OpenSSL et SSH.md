## TP2 - Chiffrement asymétrique, OpenSSL et SSH

### Introduction
Le TP aborde l’utilisation d’OpenSSL pour le chiffrement et le réseau SSH pour la sécurisation des communications. La première partie concerne la génération de clés RSA, le chiffrement et le déchiffrement des données avec AES, et la comparaison des modes de chiffrement. La seconde partie se focalise sur la configuration et l’utilisation du réseau SSH.

### Chiffrement par OpenSSL

#### Question 1
**Générer une paire de clés RSA à l’aide de OpenSSL.**

```sh
openssl genpkey -algorithm RSA -out private_key.pem
openssl rsa -pubout -in private_key.pem -out public_key.pem
```

#### Question 2
**Utiliser la clé RSA générée pour chiffrer un document texte.**

```sh
openssl rsautl -encrypt -inkey public_key.pem -pubin -in plaintext.txt -out encrypted.txt
```

#### Question 3
**Chiffrer une image en utilisant le mode de chiffrement RSA, puis AES-CBC. Que remarquez-vous ?**

```sh
openssl rsautl -encrypt -inkey public_key.pem -pubin -in image.jpg -out encrypted_image_rsa.jpg
openssl enc -aes-256-cbc -salt -in image.jpg -out encrypted_image_aes.jpg
```

### Réseau SSH (en binôme)

#### Question 4
**Configurer un nouvel utilisateur et accorder les permissions nécessaires.**

```sh
# Sous Windows
net user new_user new_password /add

# Sous Linux
sudo adduser new_user
sudo chown -R new_user:new_user /path/to/directory
```

#### Question 5
**Se connecter au réseau SSH en tant que nouvel utilisateur et créer un fichier.**

```sh
ssh new_user@server_ip_address
# Puis sur le serveur
touch new_file.txt
```

#### Question 6
**À l’aide de la commande `scp`, envoyer un fichier texte vers le serveur SSH. Utiliser à la fois un chiffrement symétrique et asymétrique.**

```sh
scp file.txt new_user@server_ip_address:/path/to/destination
# Pour envoyer un fichier chiffré avec OpenSSL
openssl rsautl -encrypt -inkey public_key.pem -pubin -in file.txt -out encrypted_file.txt
scp encrypted_file.txt new_user@server_ip_address:/path/to/destination
```