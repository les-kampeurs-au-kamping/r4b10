
## TP6 - Serveur HTTP et certificats TLS-SSL

### Introduction
Pour ce TP, nous allons créer des serveurs Apache2 et gérer les certificats TLS-SSL.

### 1. Serveur HTTP Apache2

#### Question 1
**Installer Apache2 et vérifier le statut du serveur.**

```sh
sudo apt install apache2
systemctl status apache2
```

#### Question 2
**Activer le module SSL et le site `default-ssl`, puis recharger Apache.**

```sh
sudo a2enmod ssl
sudo a2ensite default-ssl
sudo systemctl reload apache2
```

### 2. Certificats TLS-SSL

#### Question 3
**Générer un certificat X.509 auto-signé.**

```sh
openssl req -new -x509 -days 365 -nodes -out monCertificat.pem -keyout maClePrivee.pem
```

#### Question 4
**Associer le nouveau certificat au serveur `default-ssl` et redémarrer Apache2.**

1. **Déplacer le certificat et la clé privée dans le répertoire approprié.**

```sh
sudo mv monCertificat.pem /etc/ssl/certs/
sudo mv maClePrivee.pem /etc/ssl/private/
```

2. **Modifier le fichier de configuration `default-ssl.conf`.**

```sh
sudo nano /etc/apache2/sites-available/default-ssl.conf
# Mettre à jour les lignes suivantes :
# SSLCertificateFile /etc/ssl/certs/monCertificat.pem
# SSLCertificateKeyFile /etc/ssl/private/maClePrivee.pem
```

3. **Recharger Apache.**

```sh
sudo systemctl reload apache2
```

### 3. Système d’authentification et sécurité

#### Question 5
**Créer un système d’authentification pour votre serveur avec la commande `htpasswd`.**

1. **Installer `apache2-utils` si nécessaire.**

```sh
sudo apt install apache2-utils
```

2. **Créer un fichier `.htpasswd` avec des utilisateurs et mots de passe.**

```sh
sudo htpasswd -c /etc/apache2/.htpasswd utilisateur1
sudo htpasswd /etc/apache2/.htpasswd utilisateur2
```

3. **Modifier le fichier de configuration pour inclure l'authentification.**

```sh
sudo nano /etc/apache2/sites-available/default-ssl.conf
# Ajouter les lignes suivantes dans le bloc <Directory "/var/www/html">
# AuthType Basic
# AuthName "Restricted Content"
# AuthUserFile /etc/apache2/.htpasswd
# Require valid-user
```

4. **Recharger Apache.**

```sh
sudo systemctl reload apache2
```

#### Question 6
**Proposer et implémenter des mesures/protocoles pour augmenter la sécurité de votre serveur.**

1. **Désactiver les versions obsolètes de SSL/TLS.**

```sh
sudo nano /etc/apache2/mods-available/ssl.conf
# Ajouter ou modifier :
# SSLProtocol all -SSLv2 -SSLv3
```

2. **Forcer l'utilisation de HTTPS en redirigeant les requêtes HTTP.**

```sh
sudo nano /etc/apache2/sites-available/000-default.conf
# Ajouter une redirection dans le bloc <VirtualHost *:80> :
# Redirect "/" "https://yourdomain.com/"
```

3. **Configurer les en-têtes de sécurité HTTP.**

```sh
sudo nano /etc/apache2/sites-available/default-ssl.conf
# Ajouter les lignes suivantes :
# Header always set X-Content-Type-Options "nosniff"
# Header always set X-Frame-Options "DENY"
# Header always set X-XSS-Protection "1; mode=block"
# Header always set Strict-Transport-Security "max-age=31536000; includeSubDomains"
```

4. **Recharger Apache.**

```sh
sudo systemctl reload apache2
```

---