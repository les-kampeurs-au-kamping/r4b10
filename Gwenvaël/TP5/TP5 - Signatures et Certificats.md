Voici les rendus Markdown pour les TP5 et TP6 sans le nom du professeur et le nom de la matière.

---

## TP5 - Signatures et Certificats

### Introduction
Nous savons déjà utiliser OpenSSL pour faire du chiffrement à clef secrète et à clef publique ainsi que des fonctions de hachage. Aujourd’hui, nous allons explorer le maniement de signatures et de certificats numériques à l’aide des commandes issues de la liste des commandes standard de OpenSSL.

### 1. Signatures

#### Question 1
**On considère une clé privée `d = 23` et une clé publique `(e, n) = (7, 55)`. Suivez le protocole détaillé dans le cours pour envoyer un message privé et la signature RSA associée à votre binôme.**

*Génération de la signature et vérification (en utilisant des nombres comme messages)*

```sh
# Clé privée d = 23, clé publique (e, n) = (7, 55)
# Message à envoyer, par exemple 20

# Calcul de la signature s = m^d mod n
message=20
d=23
n=55
signature=$((message**d % n))
echo "Signature: $signature"

# Vérification de la signature (réception)
# Calcul de m' = s^e mod n
e=7
received_signature=$signature
verified_message=$((received_signature**e % n))
echo "Verified Message: $verified_message"
```

#### Question 2
**Grâce à OpenSSL, générer un couple clé privée/clé publique, puis générer une signature d’un message en utilisant votre clé privée.**

```sh
# Générer une paire de clés RSA
openssl genpkey -algorithm RSA -out private_key.pem -aes256
openssl rsa -pubout -in private_key.pem -out public_key.pem

# Créer un message à signer
echo "Message à signer" > message.txt

# Signer le message avec la clé privée
openssl dgst -sha256 -sign private_key.pem -out message.sig message.txt

# Vérification de la signature par votre binôme
openssl dgst -sha256 -verify public_key.pem -signature message.sig message.txt
```

### 2. Certificats

#### Question 3
**Créer un certificat au format X.509 auto-signé.**

1. **Générer votre paire de clés RSA dans un fichier nommé "maCle.pem", d’une taille de 1024 bits et protégée par un mot de passe.**

```sh
openssl genpkey -algorithm RSA -out maCle.pem -aes256 -pkeyopt rsa_keygen_bits:1024
```

2. **Créer un fichier ne contenant que la partie publique de votre clé RSA.**

```sh
openssl rsa -pubout -in maCle.pem -out maClePublique.pem
```

3. **Créer un certificat auto-signé au format X.509 valable 365 jours.**

```sh
openssl req -new -x509 -key maCle.pem -out monCertificat.pem -days 365
```

4. **Visualiser les informations du certificat créé.**

```sh
openssl x509 -in monCertificat.pem -text -noout
```

#### Question 4
**En tant qu’autorité de certification (CA), générer un certificat signé pour votre binôme.**

1. **Générer une requête de certificat.**

```sh
openssl req -new -key maCle.pem -out maRequete.csr
```

2. **En tant que CA, signer la requête de certificat reçue de votre binôme.**

```sh
openssl x509 -req -in maRequete.csr -CA monCertificat.pem -CAkey maCle.pem -CAcreateserial -out certificatSigne.pem -days 365
```

3. **Visualiser et vérifier le certificat signé.**

```sh
openssl x509 -in certificatSigne.pem -text -noout
openssl verify -CAfile monCertificat.pem certificatSigne.pem
```

---
