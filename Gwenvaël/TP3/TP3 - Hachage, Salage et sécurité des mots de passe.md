
## TP3 - Hachage, Salage et sécurité des mots de passe

### Introduction
Ce TP explore les concepts de hachage, de salage et de stockage sécurisé. L'utilisation des fonctions de hachage et la sécurisation des mots de passe à l'aide d'OpenSSL et SSH seront abordées.

### Fonctions de hachage

#### Question 1
**Utilisez OpenSSL pour générer des hachages MD5, SHA-1 et SHA-256 pour différents mots ou phrases. Comparer les longueurs des hachages générés.**

```sh
echo -n "phrase" | openssl dgst -md5
echo -n "phrase" | openssl dgst -sha1
echo -n "phrase" | openssl dgst -sha256
```

#### Question 2
**Réaliser les étapes suivantes :**

1. **Utilisez la fonction de hachage MD5 pour calculer la somme de contrôle MD5 d’un document volumineux.**

```sh
openssl dgst -md5 large_document.txt
```

2. **Modifiez le contenu du document, recalculer la somme de contrôle MD5 et observer les différences.**

```sh
# Modifier le document
echo "modification" >> large_document.txt
openssl dgst -md5 large_document.txt
```

3. **Réaliser les mêmes manipulations avec le protocole SHA-256.**

```sh
openssl dgst -sha256 large_document.txt
```

#### Question 3
**Proposer un protocole utilisant ce principe pour assurer l’authenticité d’un fichier envoyé sur un réseau.**

### Salage et sécurité des mots de passe

#### Question 4
**Créer trois fichiers texte : un avec des mots de passe, un avec chaque mot haché, et un avec chaque mot salé et haché.**

```sh
# Liste des mots de passe
echo -e "123456\nadmin\n1234\npassword\n123\nAa123456\n111111\n000000\nadmin123\nroot" > passwords.txt

# Fichier contenant chaque mot haché
while read -r password; do echo -n "$password" | openssl dgst -sha256; done < passwords.txt > hashed_passwords.txt

# Fichier contenant chaque mot salé et haché
while read -r password; do
  salt=$(openssl rand -hex 4)
  echo -n "$salt$password" | openssl dgst -sha256
done < passwords.txt > salted_hashed_passwords.txt
```

#### Question 5
**Stocker la liste des mots de passe hachés et tenter de déterminer les mots de passe originaux par attaque par tables arc-en-ciel.**

```sh
# Enregistrer la liste des mots de passe hachés sur le réseau
scp hashed_passwords.txt new_user@server_ip_address:/path/to/destination
```