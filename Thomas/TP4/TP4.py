# @@@@@@@@@@@@@@@@@@@@@@@@ 
# @         TP 4         @
# @@@@@@@@@@@@@@@@@@@@@@@@

#################################
#   Codes détecteurs d'erreurs  #
#################################

# Question 1 :  Implémenter un fonction prenant en entrée un message a et renvoyant le résultat après
#               application du code de parité.


def parity(data):

    # Initialisation du compteur de '1'
    counter_1 = 0

    for i in data:
        if i == '1' :
            counter_1 = counter_1 + 1
    
    # Si le nombre de '1' est pair
    if counter_1 % 2 == 0 :
        print("Nombre de '1' pair donc ajout de '1' à la fin du message")
        data = data + str(1)
    
    # Si le nombre de 1 est impair
    else:
        data = data + str(0)
        print("Nombre de '1' impair donc ajout de '0' à la fin du message")

    return data

print("--------------------------------------------")
print("")
print(parity("1100100"))


# Qestion 2 :   Proposer et implémenter une fonction se basant sur le code de parité et étant plus per-
#               formant pour la détection d’une multitude d’erreurs.

def hamming_code(data):

    # Calcul de la taille du message initial
    data_length = len(data)
    
    # Calcul du nombre de bits de parité nécessaires
    r = 0
    while 2**r < data_length + r + 1:
        r += 1
    
    # Insertion des bits de parité à leurs positions correctes
    encoded_data = list(data)
    for i in range(r):
        encoded_data.insert(2**i - 1, 'p')
    
    # Calcul des bits de parité
    for i in range(r):
        index = 2**i - 1
        parity = 0
        for j in range(index, len(encoded_data), 2*(i+1)):
            chunk = encoded_data[j:j+(i+1)]
            parity ^= sum(1 for bit in chunk if bit == '1') % 2
        encoded_data[index] = str(parity)
    
    return ''.join(encoded_data)

# Test de la fonction avec un message
message = "1100100"
encoded_message = hamming_code(message)
print("Message encodé avec le code de Hamming:", encoded_message)


# Qestion 3 :   Implémenter un fonction prenant en entrée un message a et un paramètre k et renvoyant
#               le résultat après application du code de k répétitions.

def repetition_code(message, k):
    repeated_message = ""
    for bit in message:
        repeated_message += bit * k
    return repeated_message

# Test de la fonction avec un message et un paramètre k
message = "1100100"
k = 3
encoded_message = repetition_code(message, k)
print("Message encodé avec le code de répétition", k, ":", encoded_message)

# Question 4 :  Implémenter un fonction prenant en entrée un message a et renvoyant le résultat de
#               après application du code de Hamming.

def hamming_code(message):

    # Calcul de la taille du message initial
    data_length = len(message)
    
    # Calcul du nombre de bits de parité nécessaires r
    r = 0
    while 2**r < data_length + r + 1:
        r += 1
    
    # Insertion des bits de parité à leurs positions correctes
    encoded_message = list(message)
    for i in range(r):
        encoded_message.insert(2**i - 1, 'p')
    
    # Calcul des bits de parité
    for i in range(r):
        index = 2**i - 1
        parity = 0
        for j in range(index, len(encoded_message), 2*(i+1)):
            chunk = encoded_message[j:j+(i+1)]
            parity ^= sum(1 for bit in chunk if bit == '1') % 2
        encoded_message[index] = str(parity)
    
    return ''.join(encoded_message)

# Test de la fonction avec un message
message = "1100100"
encoded_message = hamming_code(message)
print("Message encodé avec le code de Hamming:", encoded_message)



#################################
#  Codes correcteurs d'erreurs  #
#################################





            
    