**Prénom :** Thomas
**Nom :** LE BRETON
**Classe :** BUT INFORMATIQUE 2B

---
## Introduction

Nous savons déjà utiliser OpenSSL pour faire du chiffrement à clef secrète et à clef publique ainsi que des fonctions de hachage. Aujourd’hui, nous allons explorer le maniement de signatures et de certificats numériques à l’aide des commandes issues de la liste des commandes standard de OpenSSL

---
## Signatures

### Question 1
On considère une clé privée d = 23 et une clé public (e, n) = (7, 55). Puis, en suivant le protocole détaillé dans le cours, envoyer un message privé et la signature RSA associée à votre binôme. Une fois avoir reçu le message et la signature que votre binôme vous aura envoyé, vérifier que la signature associée au message est correcte, et donc que le message est authentique.

**h(M) -> h(M)^d [m]**

```python
>>> m = 2
>>> d = 23
>>> e = 7
>>> n = 55
>>> def rsa_sign(m, d, n):
...     return pow(m, d, n)
... 
>>> rsa_sign(m,d,n)
8
```

**S^e [m] -> h(M)**

```python
>>> m = 8
>>> e = 7
>>> n = 55
>>> def rsa_verify(m, e, n):
...     return pow(m, e, n)
... 
>>> rsa_verify(m, e, n)
2
>>> 
```

### Question 2
Grâce à OpenSSL, générer un couple `clé privée/clé publique`, puis toujours en utilisant OpenSSL, générer une signature d’un message en utilisant votre clé privée.
De même que dans la question précédente, envoyer ce couple `(message,signature)` à votre binôme, et vérifier l’authenticité du message qu’il vous envoie.

* Générer une clé privée et une clé publique avec OpenSSL
```bash
openssl genrsa -out private.pem 2048
``` 

* Générer une clé publique à partir de la clé privée
```bash
openssl rsa -in private.pem -outform PEM -pubout -out public.pem
```

* Signer un message avec la clé privée
```bash
echo "Hello World" > message.txt
openssl dgst -sha256 -sign private.pem -out signature.bin message.txt
```

---
## Certificats

### Question 3
Nous allons créer un certificat au format X.509 auto-signé. Pour cela suivez les étapes suivantes. 

1. Générer votre paire de clés RSA dans un fichier nommé `"maCle.pem"`, d’une taille de 1024 bits et protégée par un mot de passe.
    **Clé privée**
    ```bash
    openssl genrsa -aes128 -out maCle.pem 1024
    ```

2. Créer un fichier ne contenant que la partie publique de votre clé RSA. Dans la suite, on suppose ce fichier nommé `maClePublique.pem`.

```bash
    openssl rsa -in maCle.pem -outform PEM -pubout -out maClePublique.pem
    ```

3. Avec la commande `req` d’OpenSSL, et à l’aide de votre clé privée, créer un certificat auto-signé. Ce certificat devra être au format X-509 et valable 365 jours.

    **Certificat**
    ```bash
    openssl req -new -x509 -key maCle.pem -out certificat.pem -days 365
    ```

    ```bash
    Enter pass phrase for maCle.pem:
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----
    Country Name (2 letter code) [AU]:FR
    State or Province Name (full name) [Some-State]:France
    Locality Name (eg, city) []:Vannes
    Organization Name (eg, company) [Internet Widgits Pty Ltd]:IUT
    Organizational Unit Name (eg, section) []:
    Common Name (e.g. server FQDN or YOUR name) []:
    Email Address []:
    ```

En utilisant la commande `x509` d’OpenSSL, vous pouvez visualiser en clair les informations du certificat créé. Examiner son contenu.

```bash
openssl x509 -in certificat.pem -text -noout
```

```bash
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            19:b8:6f:05:84:3b:3b:b6:b8:d0:f3:0f:66:22:8f:98:2c:68:21:b5
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = FR, ST = France, L = Vannes, O = IUT, OU = BUT INFO, CN = LE BRETON, emailAddress = zad@azd.fr
        Validity
            Not Before: May 14 12:34:32 2024 GMT
            Not After : May 14 12:34:32 2025 GMT
        Subject: C = FR, ST = France, L = Vannes, O = IUT, OU = BUT INFO, CN = LE BRETON, emailAddress = zad@azd.fr
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (1024 bit)
                Modulus:
                    00:e4:a1:c4:7a:39:bd:a1:16:01:f5:f6:45:15:b1:
                    5e:5f:b0:85:b2:eb:aa:b2:27:be:2d:91:76:4d:ba:
                    c7:00:93:2e:34:c2:53:68:df:d5:36:4f:99:81:bf:
                    40:6b:72:b4:6a:eb:c0:eb:aa:ca:e6:d3:33:35:bc:
                    f1:2d:5c:a6:a2:6d:fb:12:52:39:85:c8:a5:68:67:
                    46:00:db:65:1c:17:5c:96:f0:13:2d:70:ff:03:5d:
                    89:3d:b5:7c:af:eb:59:1a:b0:d8:79:89:d7:0a:d0:
                    7e:4b:c1:43:10:d3:a6:8f:cf:47:ad:33:74:59:40:
                    36:fd:e4:e1:6d:d0:88:fd:eb
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier: 
                79:5B:32:A4:24:0F:96:0D:67:22:0B:53:65:85:C0:2B:51:0E:11:2C
            X509v3 Authority Key Identifier: 
                79:5B:32:A4:24:0F:96:0D:67:22:0B:53:65:85:C0:2B:51:0E:11:2C
            X509v3 Basic Constraints: critical
                CA:TRUE
    Signature Algorithm: sha256WithRSAEncryption
    Signature Value:
        c5:48:68:d5:9b:f6:a9:02:d0:c4:8d:91:fd:e5:4f:af:f4:cf:
        9c:b6:b9:19:62:72:47:e6:0e:58:a1:12:b6:aa:4c:33:40:b1:
        34:ce:48:55:60:aa:cb:ab:21:f7:ce:65:0d:e1:ea:85:d6:f1:
        6a:bb:22:d3:76:28:48:3c:f9:31:52:09:6e:dd:5e:ef:68:ab:
        2f:a6:41:d2:b7:e9:54:ba:4a:21:9a:41:99:65:79:a6:0b:49:
        20:ba:b7:ac:74:07:44:bb:ed:0a:92:8c:71:53:f4:14:48:1e:
        48:29:8d:59:5e:30:51:99:cb:6b:12:e4:9c:81:a7:ac:bf:3e:
        4f:b2
```

### Question 4
Dans cette question vous allez endosser le rôle d’autorité de certification (CA). Pour cela vous allez travailler en binôme.

En plus de générer des certificats auto-signés, la fonction `req` d’OpenSSL permet également de générer un requête d’un utilisateur à transmettre au CA.
- En tant qu’utilisateur, générer un telle requête grâce à la commande `req` et votre clé privée, puis envoyez-là à votre binôme. (Vous pouvez visualiser cette requête, qui n’est pas un certificat).
    ```bash
    openssl req -new -key maCle.pem -out maRequete.pem
    ```

    ```bash
    Enter pass phrase for maCle.pem:
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----
    Country Name (2 letter code) [AU]:FR
    State or Province Name (full name) [Some-State]:France
    Locality Name (eg, city) []:
    Organization Name (eg, company) [Internet Widgits Pty Ltd]:
    Organizational Unit Name (eg, section) []:
    Common Name (e.g. server FQDN or YOUR name) []:
    Email Address []:

    Please enter the following 'extra' attributes
    to be sent with your certificate request
    A challenge password []:1234
    An optional company name []:
    ```

- En tant que CA, grâce à la commande ca d’OpenSSL, répondre à la requête reçue de votre binôme en lui fournissant un certificat avec votre signature numérique (généré à partir de votre clé privé).
    ```bash
    openssl ca -in maRequete.pem -out certificat.bin
    ```

    ```bash
Using configuration from /usr/lib/ssl/openssl.cnf 
Could not open file or uri for loading CA private key from ./demoCA/private/cakey.pem: No such file or directory
    ```

Vous venez de recevoir votre certificat fraîchement signé par votre binôme-CA. Visualisez-le avec la commande `x509`. Vous pouvez vérifier au moyen de la commande `verify` la validité de votre propre certificat (nécessitant un certificat auto-signé du CA-binôme).

```bash
openssl x509 -in certificat.bin -text -noout
```