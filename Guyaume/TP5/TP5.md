# TP5 - Signatures et certificats

## Question 1 : On considère une clé privé d = 23 et une clé public (e, n) = (7, 55). Puis, en suivant le protocole détaillé dans le cours, envoyer un message privé et la signature RSA associé à votre binôme. Un fois avoir reçu le message et la signature que votre binôme vous aura envoyé, vérifier que la signature associé au message est correcte, et donc que le message est authentique.

```python
def rsa_encrypt(m, e, n):
    return pow(m, e, n)

def rsa_decrypt(m, d, n):
    return pow(m, d, n)

def hash(m):
    sum = 0
    for _ in range(len(str(m))):
        sum += int(str(m)[_])
    return sum

m = 4654742  # Message
d = 23  # Clé privée
e = 7   # Clé publique
n = 55  # Modulo

# Signature d'un message
h1 = hash(m)
s = rsa_encrypt(h1, d, n)

print("====== Signature du message ======")
print("Message:", m)
print("Signature:", s)

# Vérification de la signature
h2 = hash(m)
h3 = rsa_decrypt(s, e, n)
valid = "Non"

if h2==h3:
    valid = "Oui"

print()
print("====== Vérification de la signature ======")
print("Message:", m)
print("Hash calculé:", h2)
print("Hash signature déchiffée:", h3)
print("Signature valide:", valid)
```

```
====== Signature du message ======
Message: 4654742
Signature: 43

====== Vérification de la signature ======
Message: 4654742
Hash calculé: 32
Hash signature déchiffée: 32
Signature valide: Oui
```

## Question 2 : Grâce à OpenSSL, générer un couple clé privé clé public, puis toujours en utilisant OpenSSL, générer une signature d’un message en utilisant votre clé privé. De même que dans la question précédente, envoyer ce couple (message,signature) à votre binôme, et vérifier l’authenticité du message qu’il vous envoie.

Génération du couple de clés

```bash
openssl genrsa -out private.pem 2048
openssl rsa -in private.pem -outform PEM -pubout -out public.pem
```

Signature d'un message

```bash
echo "Hello World" > message.txt
openssl dgst -sha256 -sign private.pem -out signature.bin message.txt
```

Vérification de la signature

```bash
openssl dgst -sha256 -verify public.pem -signature signature.bin message.txt
```

```
Verified OK
```

## Question 3.

Nous allons créer un certificat au format X.509 auto-signé. Pour cela suivez les étapes suivantes.

1. Générer votre paire de clés RSA dans un fichier nommé "maCle.pem", d’une taille de 1024 bits et protégée par un mot de passe.

```bash
openssl genrsa -aes128 -out maCle.pem 1024
```

2. Créer un fichier ne contenant que la partie publique de votre clé RSA. Dans la suite, on suppose ce fichier nommé maClePublique.pem.

```bash
openssl rsa -in maCle.pem -outform PEM -pubout -out maClePublique.pem
```

3. Avec la commande req d’OpenSSL, et à l’aide de votre clé privé, créer un certificat auto-signé. Ce certificat devra être au format X-509 et valable 365 jours. En utilisant la commande x509 d’OpenSSL, vous pouvez visualiser en clair les informations du certificat créé. Examiner son contenu.

```bash
openssl req -new -x509 -key maCle.pem -out certificat.pem -days 365