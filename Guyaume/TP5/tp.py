def rsa_encrypt(m, e, n):
    return pow(m, e, n)

def rsa_decrypt(m, d, n):
    return pow(m, d, n)

def hash(m):
    sum = 0
    for _ in range(len(str(m))):
        sum += int(str(m)[_])
    return sum

m = 4654742  # Message
d = 23  # Clé privée
e = 7   # Clé publique
n = 55  # Modulo

# Signature d'un message
h1 = hash(m)
s = rsa_encrypt(h1, d, n)

print("====== Signature du message ======")
print("Message:", m)
print("Signature:", s)

# Vérification de la signature
h2 = hash(m)
h3 = rsa_decrypt(s, e, n)
valid = "Non"

if h2==h3:
    valid = "Oui"

print()
print("====== Vérification de la signature ======")
print("Message:", m)
print("Hash calculé:", h2)
print("Hash signature déchiffée:", h3)
print("Signature valide:", valid)