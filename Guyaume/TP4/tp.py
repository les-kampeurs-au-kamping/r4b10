print("====== TP4 ======")
print("Guyaume")
print("MORICE")
print("INFO2B2")

# Question 1. Implémenter un fonction prenant en entrée un message a et renvoyant le résultat après
# application du code de parité.
def encode_parity(data):
    sum = 0
    for c in data:
        if c == '1':
            sum += 1
    return data + str(sum % 2)

def decode_parity(data):
    sum = 0
    for c in data[:-1]:
        if c == '1':
            sum += 1
    if sum % 2 == int(data[-1]):
        return data[:-1]
    else:
        return "error"

print()
print("encode_parity(\"1100101\") = \"" + encode_parity("1100101") + "\"")
print("encode_parity(\"1100100\") = \"" + encode_parity("1100100") + "\"")
print("decode_parity(\"11010001\") = \"" + decode_parity("11010001") + "\"")
print("decode_parity(\"11001000\") = \"" + decode_parity("11001000") + "\"")

# Question 2. Proposer et implémenter une fonction se basant sur le code de parité et étant plus per-
# formant pour la détection d’une multitude d’erreurs.

def encode_parity2(data, n):
    data_encoded = ""
    for i in range(0, len(data), n):
        data_encoded += encode_parity(data[i:i+n])
    return data_encoded

def decode_parity2(data, n):
    data_decoded = ""
    for i in range(0, len(data), n+1):
        tmp = decode_parity(data[i:i+n+1])
        if tmp == "error":
            return "error"
        else:
            data_decoded += tmp
    return data_decoded

print()
print("encode_parity2(\"1100101\", 2) = \"" + encode_parity2("1100101", 2) + "\"")
print("encode_parity2(\"1100100\", 2) = \"" + encode_parity2("1100100", 2) + "\"")
print("decode_parity2(\"110110011001001\", 4) = \"" + decode_parity2("110110011001001", 4) + "\"")
print("decode_parity2(\"110010001101001\", 4) = \"" + decode_parity2("110010001101001", 4) + "\"")

# Question 3. Implémenter un fonction prenant en entrée un message a et un paramètre k et renvoyant
# le résultat après application du code de k répétitions.

def encode_repetition(data, k):
    return data * k

def decode_repetition(data, k):
    return data[0:len(data):k]

print()
print("encode_repetition(\"1100101\", 3) = \"" + encode_repetition("1100101", 3) + "\"")
print("encode_repetition(\"1100100\", 3) = \"" + encode_repetition("1100100", 3) + "\"")
print("decode_repetition(\"111000111111\", 3) = \"" + decode_repetition("111000111111", 3) + "\"")
print("decode_repetition(\"111000111110\", 3) = \"" + decode_repetition("111000111110", 3) + "\"")

# Question 4. Implémenter un fonction prenant en entrée un message a et renvoyant le résultat de
# après application du code de Hamming.

def encode_hamming(data):
    u3 = data[2]
    u5 = data[5]
    u6 = data[6]
    u7 = data[7]
    u1 = str(int(u3) ^ int(u5) ^ int(u7))
    u2 = str(int(u3) ^ int(u6) ^ int(u7))
    u4 = str(int(u5) ^ int(u6) ^ int(u7))
    return u1 + u2 + u3 + u4 + u5 + u6 + u7

# Question 5. Supposons que l’on reçoit un message v condition par un code à k répétition et qui a pu
# être altéré lors de l’envoie. Implémenter une fonction permettant de retrouver le message originel u.

# Question 6. Supposons que l’on reçoit un message v de 7 bits conditionné par un code de Hamming
# et qui a pu être altéré de 1 bit lors de l’envoie. Implémenter une fonction permettant de retrouver le
# message originel u.

# Question 7. Même question pour un message v de taille quelconque dans lequel au plus 1 bit est
# altéré tous les 7 bits.