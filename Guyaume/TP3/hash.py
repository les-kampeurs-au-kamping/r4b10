import hashlib
import random

def hash_string(string):
    return hashlib.sha256(string.encode()).hexdigest()

def simple_hash_password():
    input_file = open("passwords.txt", "r")
    output_file = open("hashes.txt", "w")

    for line in input_file:
        output_file.write(hash_string(line[:-1]) + "\n")

    input_file.close()
    output_file.close()

def salt_hash_password():
    input_file = open("passwords.txt", "r")
    output_file = open("salted_hashes.txt", "w")
    alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

    for line in input_file:
        salt = ""
        for i in range(16):
            salt += random.choice(alphabet)
        output_file.write(salt + " " + hash_string(salt + line) + "\n")

    input_file.close()
    output_file.close()

def generate_rainbow_table():
    input_file = open("rockyou.txt", "r")
    output_file = open("rainbow_table.txt", "w")

    for line in input_file:
        output_file.write(line[:-1] + " " + hash_string(line[:-1]) + "\n")

    input_file.close()
    output_file.close()

def find_password():
    input_file_hashes = open("hashes.txt")
    input_file_rainbow_table = open("rainbow_table.txt")

    rainbow_table = []
    for line in input_file_rainbow_table:
        password, hash = line.split(" ")
        rainbow_table.append((password, hash[:-1]))

    for line in input_file_hashes:
        for password, hash in rainbow_table:
            if line[:-1] == hash:
                print("Found password :", password)
                print("Hash associated :", hash)
                return password


simple_hash_password()
salt_hash_password()
generate_rainbow_table()

find_password()