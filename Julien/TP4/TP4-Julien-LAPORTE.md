*LAPORTE Julien*

# Introduction
Dans le domaine des communications numériques, la fiabilité de la transmission des données est essentielle. Les codes correcteurs d’erreurs sont des outils fondamentaux pour détecter et corriger les erreurs qui peuvent survenir lors de la transmission de données numériques sur des canaux perturbés.

Dans ce TP, nous explorerons différentes techniques de correction d’erreurs, notamment le code de parité, le code de répétition, et le code de Hamming, et nous implémenterons des solutions pour encoder et décoder des messages en utilisant ces codes.

Vous pouvez utiliser les outils que vous souhaitez pour réaliser ce TP

## 1 Code détecteur d’erreur

> **Question 1.** *Implémenter un fonction prenant en entrée un message a et renvoyant le résultat après application du code de parité*

```python
def code_parite(message):
    nb_bits_1 = 0
	
    for bit in message:
        if bit == '1':
            nb_bits_1 += 1
            
    if nb_bits_1 % 2 == 0:
        message += '0'
    else:
        message += '1'
        
    return message
```

input
```bash
010010111111100001100010011001010110111001101000011000010111011001101110
```

output
```bash
0100101111111000011000100110010101101110011010000110000101110110011011101
```

> **Question 2.** *Proposer et implémenter une fonction se basant sur le code de parité et étant plus performant pour la détection d’une multitude d’erreurs.*

```python

```

input
```bash

```

output
```bash

```

> **Question 3.** *Implémenter un fonction prenant en entrée un message a et un paramètre k et renvoyant le résultat après application du code de k répétitions.*

```python
def code_repetition(message, k):
    message_repetition = ""
    for bit in message:
        message_repetition += bit * k
        
    return message_repetition
```

input
```bash
010010111111100001100010011001010110111001101000011000010111011001101110
```

output
```bash
000111000000111000111111111111111111111000000000000111111000000000111000000111111000000111000111000111111000111111111000000111111000111000000000000111111000000000000111000111111111000111111000000111111000111111111000
```

> **Question 4.** *Implémenter un fonction prenant en entrée un message a et renvoyant le résultat de après application du code de Hamming.*

```python
def code_hamming(message):
	
```

input
```bash

```

output
```bash

```

## 2 Code correcteur d’erreur

> **Question 4.** *Supposons que l’on reçoit un message v condition par un code à k répétition et qui a pu être altéré lors de l’envoie. Implémenter une fonction permettant de retrouver le message originel u.*

```python

```

input
```bash

```

output
```bash

```

> **Question 5.** *Supposons que l’on reçoit un message v de 7 bits conditionné par un code de Hamming et qui a pu être altéré de 1 bit lors de l’envoie. Implémenter une fonction permettant de retrouver le message originel u.*

```python

```

input
```bash

```

output
```bash

```