*LAPORTE Julien*

## Introduction
Nous savons déjà utiliser OpenSSL pour faire du chiffrement à clef secrète et à clef publique ainsi que des fonctions de hachage. Aujourd’hui, nous allons explorer le maniement de signatures et de certificats numériques à l’aide des commandes issues de la liste des commandes standard de OpenSSL

## 1 Signatures

### Question 1.
> On considère une clé privé d = 23 et une clé public (e, n) = (7, 55). Puis, en suivant le protocole détaillé
dans le cours, envoyer un message privé et la signature RSA associé à votre binôme.
Un fois avoir reçu le message et la signature que votre binôme vous aura envoyé, vérifier que la signature associé au message est correcte, et donc que le message est authentique.

**h(M) -> h(M)^d [m]**

```python
>>> m=2
>>> d=23
>>> e=7
>>> n=55
>>> def rsa_sign(m, d, n):
...     return pow(m, d, n)
... 
>>> rsa_sign(m,d,n)
8
```

**S^e [m] -> h(M)**

```python
>>> m=8
>>> e=7
>>> n=55
>>> def rsa_verify(m, e, n):
...     return pow(m, e, n)
... 
>>> rsa_verify(m, e, n)
2
>>> 
```

### Question 2.
> Grâce à OpenSSL, générer un couple clé privé clé public, puis toujours en utilisant OpenSSL, générer
une signature d’un message en utilisant votre clé privé.
De même que dans la question précédente, envoyer ce couple (message,signature) à votre binôme, et vérifier l’authenticité du message qu’il vous envoie.

* Générer une clé privée et une clé publique avec OpenSSL
```bash
openssl genrsa -out private.pem 2048
``` 

* Générer une clé publique à partir de la clé privée
```bash
openssl rsa -in private.pem -outform PEM -pubout -out public.pem
```

* Signer un message avec la clé privée
```bash
echo "Hello World" > message.txt
openssl dgst -sha256 -sign private.pem -out signature.bin message.txt
```

## 2 Certificats

### Question 3.
> Nous allons créer un certificat au format X.509 auto-signé. Pour cela suivez les étapes suivantes. 
1. Générer votre paire de clés RSA dans un fichier nommé "maCle.pem", d’une taille de 1024 bits
et protégée par un mot de passe.<br>
    **Private Key**
    ```bash
    openssl genrsa -aes128 -out maCle.pem 1024
    ```

2. Créer un fichier ne contenant que la partie publique de votre clé RSA. Dans la suite, on suppose ce fichier nommé maClePublique.pem.<br>
    **Public Key**
    ```bash
    openssl rsa -in maCle.pem -outform PEM -pubout -out maClePublique.pem
    ```

3. Avec la commande req d’OpenSSL, et à l’aide de votre clé privé, créer un certificat auto-signé.
Ce certificat devra être au format X-509 et valable 365 jours.<br>
    **Certificate**
    ```bash
    openssl req -new -x509 -key maCle.pem -out certificat.pem -days 365
    ```

    ```bash
    Enter pass phrase for maCle.pem:
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----
    Country Name (2 letter code) [AU]:FR
    State or Province Name (full name) [Some-State]:France
    Locality Name (eg, city) []:Vannes
    Organization Name (eg, company) [Internet Widgits Pty Ltd]:IUT
    Organizational Unit Name (eg, section) []:
    Common Name (e.g. server FQDN or YOUR name) []:
    Email Address []:
    ```

En utilisant la commande x509 d’OpenSSL, vous pouvez visualiser en clair les informations du certificat créé. Examiner son contenu.

```bash
openssl x509 -in certificat.pem -text -noout
```

```bash
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            5a:27:64:af:9d:30:35:36:60:28:e8:1f:c2:05:ac:36:85:a7:fd:1a
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = FR, ST = France, L = Vannes, O = IUT
        Validity
            Not Before: May 14 10:36:14 2024 GMT
            Not After : May 14 10:36:14 2025 GMT
        Subject: C = FR, ST = France, L = Vannes, O = IUT
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (1024 bit)
                Modulus:
                    00:b2:57:28:2f:f2:29:f1:da:58:f5:02:ae:c4:c3:
                    2e:c4:e0:21:c9:49:75:9d:75:6d:a8:55:6b:99:22:
                    9d:14:c5:c4:fb:52:28:2f:d2:83:a3:84:0b:a3:2e:
                    c4:70:86:68:2b:8a:b6:13:5d:e0:8f:ca:5e:eb:fe:
                    ab:d9:f8:73:15:2a:2d:a1:15:2f:c2:0e:b4:9d:2b:
                    e7:56:a8:8f:c8:0c:e5:32:7a:41:91:6c:02:e9:70:
                    74:84:ee:19:0a:47:ff:7f:88:e3:3c:24:47:3e:56:
                    eb:1f:a8:28:4e:d2:3c:df:d5:1e:f0:46:fb:cf:8e:
                    e9:55:8f:fe:28:93:47:fd:f5
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier: 
                74:77:94:3D:02:47:02:B1:24:87:9C:3D:5A:41:1B:C7:6B:72:69:CA
            X509v3 Authority Key Identifier: 
                74:77:94:3D:02:47:02:B1:24:87:9C:3D:5A:41:1B:C7:6B:72:69:CA
            X509v3 Basic Constraints: critical
                CA:TRUE
    Signature Algorithm: sha256WithRSAEncryption
    Signature Value:
        14:06:46:f7:1b:60:d4:45:ec:03:4c:ec:53:34:18:81:06:cc:
        9f:4c:fe:c4:fd:19:04:c2:2c:ba:7a:13:8b:43:17:aa:45:95:
        28:68:8c:c0:57:19:e1:fe:23:a5:8c:7b:46:8e:c4:ea:58:8d:
        a8:49:05:bc:ce:fc:2f:87:7b:8d:f6:bd:79:b4:0e:89:26:e7:
        f7:d3:12:ff:f9:0e:17:a5:5c:38:10:dc:e7:29:53:3e:f8:15:
        c5:2a:94:a5:49:b0:b3:bf:67:f4:45:35:f9:0f:cd:cc:89:ff:
        86:bd:32:71:40:be:0b:6e:0c:89:c1:55:bc:a8:45:fa:fb:6d:
        e8:6b
```

### Question 4.
Dans cette question vous allez endosser le rôle d’autorité de certification (CA). Pour cela vous allez travailler en binôme.

En plus de générer des certificats auto-signé, la fonction req d’OpenSSL permet également de générer un requête d’un utilisateur à transmettre au CA.
- En tant qu’utilisateur, générer un telle requête grâce à la commande req et votre clé privé, puis envoyer là à votre binôme. (Vous pouvez visualiser cette requête, qui n’est pas un certificat).
    ```bash
    openssl req -new -key maCle.pem -out maRequete.pem
    ```

    ```bash
    Enter pass phrase for maCle.pem:
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----
    Country Name (2 letter code) [AU]:FR
    State or Province Name (full name) [Some-State]:France
    Locality Name (eg, city) []:
    Organization Name (eg, company) [Internet Widgits Pty Ltd]:
    Organizational Unit Name (eg, section) []:
    Common Name (e.g. server FQDN or YOUR name) []:
    Email Address []:

    Please enter the following 'extra' attributes
    to be sent with your certificate request
    A challenge password []:1234
    An optional company name []:
    ```

- En tant que CA, grâce à la commande ca d’OpenSSL, répondre à la requête reçue de votre binôme en lui fournissant un certificat avec votre signature numérique (généré à partir de votre clé privé).
    ```bash
    openssl ca -in maRequete.pem -out certificat.bin
    ```

    ```
    Using configuration from /usr/lib/ssl/openssl.cnf
    Could not open file or uri for loading CA private key from ./demoCA/private/cakey.pem: No such file or directory
    ```

Vous venez de recevoir votre certificat fraîchement signé par votre binôme-CA. Visualisez-le avec la commande x509. Vous pouvez vérifier au moyen de la commande verify la validité de votre propre certificat (nécessitant un certificat autosigné du CA-binôme)
```bash
openssl x509 -in certificat.bin -text -noout
```