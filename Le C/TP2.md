### TP2 - Chiffrement asymétrique, Openssl et SSH

#### Introduction

Le TP aborde l’utilisation d’OpenSSL pour le chiffrement et le réseau SSH pour la sécurisation des communications. 

- **Première partie :** Générer des clés RSA, chiffrer et déchiffrer des données avec AES, et comparer ces modes de chiffrement.
- **Seconde partie :** Configuration et utilisation du réseau SSH. Création d'un réseau sécurisé, gestion des utilisateurs et leurs autorisations, échange de fichiers sécurisés.

Une annexe est disponible pour les commandes à réaliser dans le terminal. La lecture de la documentation est fortement encouragée.

#### 1. Chiffrement par OpenSSL

**Pré-requis :** Assurez-vous d’avoir installé OpenSSL.

##### Question 1. Générer une paire de clés RSA à l’aide de OpenSSL.

- **Commande :**
  ```sh
  openssl genpkey -algorithm RSA -out private_key.pem
  openssl rsa -pubout -in private_key.pem -out public_key.pem
  ```

##### Question 2. Utiliser la clé RSA générée pour chiffrer un document texte.

- **Commande :**
  ```sh
  openssl rsautl -encrypt -inkey public_key.pem -pubin -in plaintext.txt -out encrypted.txt
  ```

##### Question 3. Chiffrer une image en utilisant le mode de chiffrement RSA, puis AES-CBC, que remarquez-vous ?

- **Commandes :**
  - **Chiffrement RSA :**
    ```sh
    openssl rsautl -encrypt -inkey public_key.pem -pubin -in image.jpg -out encrypted_image_rsa.jpg
    ```
  - **Chiffrement AES-CBC :**
    ```sh
    openssl enc -aes-256-cbc -salt -in image.jpg -out encrypted_image_aes.jpg
    ```
- **Remarque :**
  - RSA est principalement utilisé pour chiffrer de petites quantités de données en raison de sa lenteur relative par rapport à AES, qui est plus adapté pour le chiffrement de fichiers volumineux comme des images.

#### 2. Réseau SSH (en binôme)

**Pré-requis :** Un réseau SSH doit être configuré sur votre machine.

##### Question 4. Configurer un nouvel utilisateur. Accorder à cet utilisateur les permissions nécessaires pour créer et modifier des fichiers dans un répertoire spécifique.

- **Commandes :**
  - **Ajouter un utilisateur :**
    ```sh
    sudo adduser new_user
    ```
  - **Accorder des permissions :**
    ```sh
    sudo chown -R new_user:new_user /path/to/directory
    ```

##### Question 5. Se connecter au réseau SSH en tant que nouvel utilisateur et créer un fichier. Ensuite, chiffrer ce fichier.

- **Commandes :**
  - **Connexion SSH :**
    ```sh
    ssh new_user@server_ip_address
    ```
  - **Créer un fichier :**
    ```sh
    touch new_file.txt
    ```
  - **Chiffrer le fichier :**
    ```sh
    openssl enc -aes-256-cbc -salt -in new_file.txt -out encrypted_file.txt
    ```

##### Question 6. À l’aide de la commande scp, envoyer un fichier texte vers le serveur SSH. En utilisant à la fois un chiffrement symétrique et asymétrique, envoyer un fichier chiffré à votre binôme pour qu’il puisse le déchiffrer.

- **Commandes :**
  - **Envoyer un fichier avec `scp` :**
    ```sh
    scp file.txt new_user@server_ip_address:/path/to/destination
    ```
  - **Chiffrement symétrique :**
    ```sh
    openssl enc -aes-256-cbc -salt -in file.txt -out encrypted_file.txt
    ```
  - **Chiffrement asymétrique :**
    ```sh
    openssl rsautl -encrypt -inkey public_key.pem -pubin -in file.txt -out encrypted_file_rsa.txt
    ```

### Annexe - Commandes

#### 3.1 Chiffrement grâce à OpenSSL

- **Générer une paire de clés RSA, chiffrer et déchiffrer avec RSA :**
  ```sh
  openssl genpkey -algorithm RSA -out private_key.pem
  openssl rsa -pubout -in private_key.pem -out public_key.pem
  ```

- **Chiffrer un fichier avec AES CBC :**
  ```sh
  openssl enc -aes-256-cbc -salt -in input.jpg -out encrypted_image.jpg
  ```

#### 3.2 Réseau SSH

- **Configurer un nouvel utilisateur (Windows/Linux) :**
  - Windows:
    ```sh
    net user new_user new_password /add
    ```
  - Linux:
    ```sh
    sudo adduser new_user
    ```

- **Accorder des permissions à l’utilisateur pour un répertoire spécifique (Windows/Linux) :**
  - Windows:
    ```sh
    icacls C:\path\to\directory /grant new_user:(OI)(CI)F /T
    ```
  - Linux:
    ```sh
    sudo chown -R new_user:new_user /path/to/directory
    ```

- **Se connecter au serveur SSH en tant que nouvel utilisateur :**
  ```sh
  ssh new_user@server_ip_address
  ```

- **Créer un fichier sur le serveur SSH (Windows/Linux) :**
  - Windows:
    ```sh
    echo > new_file.txt
    ```
  - Linux:
    ```sh
    touch new_file.txt
    ```

- **Utiliser `scp` pour envoyer un fichier vers le serveur SSH :**
  ```sh
  scp file.txt new_user@server_ip_address:/path/to/destination
  ```