### Fiche de Révision : Cryptographie et Sécurité

#### 1. Rappel sur la Cryptographie Symétrique

##### Principe
- **K** : la clé
- **M** : le message en clair
- **C** : le message chiffré

```mermaid
graph TD;
    A[Message en clair M] -->|Chiffrement avec K| B[Message chiffré C];
    B -->|Déchiffrement avec K| A;
```

##### Caractéristiques
- Chiffrement rapide, particulièrement en implémentation matérielle.
- Clés courtes : 128 - 256 bits (comparé à RSA : 1024 - 2048 bits).
- Inconvénient majeur : partage de la clé commune K.

##### Chiffrement à flot ou par bloc

**Chiffrement à flot**
- Génération d'une suite pseudo-aléatoire de même longueur que les données.
- Combinaison avec les données par XOR bit-à-bit.
- Exemples : WEP, WPA, WPA2, E0 (Bluetooth), A5 (GSM).

```mermaid
graph TD;
    A[Clé (K)] -->|Génération de| B[Keystream];
    B -->|XOR avec| C[Message en clair];
    C --> D[Message chiffré];
```

**Chiffrement par bloc**
- Découpage des données en blocs de taille fixe (64 ou 128 bits).
- Chiffrement de chaque bloc séparément, puis combinaison selon un mode opératoire (ECB, CBC, CTR).
- Exemples : AES (Advanced Encryption Standard), DES (64 bits, clés 56 bits).

```mermaid
graph TD;
    A[Message en clair] -->|Découpage en| B[Blocs];
    B -->|Chiffrement de chaque bloc| C[Blocs chiffrés];
    C -->|Combinaison| D[Message chiffré];
```

##### Sécurité du chiffrement à flot
- Problèmes si plusieurs messages sont chiffrés avec la même clé.
- Nécessité d'un générateur pseudo-aléatoire de bonne qualité.
- Faiblesses prouvées par les attaques sur des algorithmes comme WEP.

```python
# Exemple de chiffrement par bloc en Python avec AES
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
from Crypto.Random import get_random_bytes

data = b"This is a secret message"
key = get_random_bytes(16)  # Clé de 128 bits
cipher = AES.new(key, AES.MODE_CBC)
ct_bytes = cipher.encrypt(pad(data, AES.block_size))
iv = cipher.iv  # Initialisation Vector
print("Chiffrement par bloc (AES-CBC):", ct_bytes)
```

#### 2. Cryptographie Asymétrique et OpenSSL

##### Rappel sur le chiffrement RSA
- Basé sur la difficulté de factoriser de grands nombres.
- Utilisation d'une clé publique (kpub) pour chiffrer et d'une clé privée (kpriv) pour déchiffrer.

```mermaid
sequenceDiagram
    participant Alice
    participant Bob
    Alice->>Bob: Envoie kpub
    Alice->>Alice: Chiffre M avec kpub
    Alice->>Bob: Envoie C
    Bob->>Bob: Déchiffre C avec kpriv
```

**Processus RSA**
- Bob possède kpriv et kpub.
- Alice chiffre le message M avec kpub pour obtenir C.
- Bob déchiffre C avec kpriv pour retrouver M.

```python
# Exemple de génération de clés RSA et chiffrement en Python avec PyCryptodome
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Random import get_random_bytes

# Génération des clés
key = RSA.generate(2048)
private_key = key.export_key()
public_key = key.publickey().export_key()

# Chiffrement
message = b'This is a secret message'
cipher = PKCS1_OAEP.new(RSA.import_key(public_key))
ciphertext = cipher.encrypt(message)

# Déchiffrement
decipher = PKCS1_OAEP.new(RSA.import_key(private_key))
decrypted_message = decipher.decrypt(ciphertext)
print("Message déchiffré:", decrypted_message)
```

##### OpenSSL
- Bibliothèque open-source pour SSL/TLS.
- Supporte AES, RSA, etc.
- Génération de clés, certificats X.509, CSR.
- Sécurisation des connexions HTTP, SMTP, IMAP.
- Signatures et vérifications numériques.

#### 3. Hachage, salage et sécurité des mots de passe

##### Fonction à sens unique
- Fonction facile à calculer mais difficile à inverser.
- Exemples de fonctions de hachage : MD5, SHA-1, SHA-256.

```mermaid
graph LR
    A[Donnée d'entrée] --> B[Fonction de hachage]
    B --> C[Empreinte fixe]
```

**Exemple de hachage en Python**
```python
import hashlib

message = "Bonjour"
md5_hash = hashlib.md5(message.encode()).hexdigest()
sha1_hash = hashlib.sha1(message.encode()).hexdigest()
sha256_hash = hashlib.sha256(message.encode()).hexdigest()

print("MD5:", md5_hash)
print("SHA-1:", sha1_hash)
print("SHA-256:", sha256_hash)
```

##### Salage des mots de passe
- Prévention contre les attaques par tables arc-en-ciel.
- Concatenation d'un sel aléatoire avec le mot de passe avant hachage.

```mermaid
graph TD
    A[Mot de passe] --> B[Concaténation avec Sel]
    B --> C[Hachage]
    C --> D[Stockage dans la base de données]
```

```python
import os
import hashlib

password = "motdepasse"
salt = os.urandom(16)  # Génération d'un sel aléatoire
hashed_password = hashlib.sha256(password.encode() + salt).hexdigest()

print("Mot de passe salé et haché:", hashed_password)
```

#### 4. Codes correcteurs d'erreurs

##### Définition
- Ensemble de règles algorithmiques pour détecter et corriger les erreurs de transmission.

```mermaid
graph TD
    A[Donnée à transmettre] --> B[Ajout de bits de contrôle]
    B --> C[Donnée transmise]
    C --> D[Donnée reçue]
    D --> E[Détection d'erreurs]
    E --> F[Correction d'erreurs]
```

**Exemple de code de Hamming**
- Codage de 4 bits de données en 7 bits de code.
- Utilisation de bits de parité pour détecter et corriger les erreurs.

```mermaid
graph TD
    A[Message original 4 bits] --> B[Ajout de bits de parité 7 bits]
    B --> C[Message transmis]
    C --> D[Message reçu avec erreurs]
    D --> E[Calcul du syndrome]
    E --> F[Correction des erreurs]
```

```python
def hamming_encode(data):
    # data: 4 bits input as a string (e.g., "1101")
    d = [int(bit) for bit in data]
    p1 = d[0] ^ d[1] ^ d[3]
    p2 = d[0] ^ d[2] ^ d[3]
    p3 = d[1] ^ d[2] ^ d[3]
    return [p1, p2, d[0], p3, d[1], d[2], d[3]]

encoded = hamming_encode("1101")
print("Code Hamming:", encoded)
```

```mermaid
graph TD
    A[Message original: 1101] --> B[Bits de parité calculés: 1010101]
    B --> C[Message transmis]
    C --> D[Message reçu: avec erreur]
    D --> E[Calcul du syndrome]
    E --> F[Correction des erreurs]
```

**Codes correcteurs d'erreurs utilisés aujourd'hui**
- Codes de Reed-Solomon : CD, DVD, QR codes.
- Codes BCH : communications numériques, cartes à puce.
- Codes convolutifs : réseaux mobiles, satellites.

### Conclusion
Cette fiche couvre en profondeur les principaux concepts de cryptographie et de sécurité discutés dans le cours. Utilisez les exemples de code et les diagrammes pour comprendre la mise en œuvre pratique de ces concepts. Bonne révision et bon contrôle!